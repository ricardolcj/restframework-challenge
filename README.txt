First time using Restfull Framework, developed for a coding challenge. This program uses a IP API to return GEO information given a IP adress, and saves previous researches for later consultation.

python3 manage.py runserver
"http://120.0.0.1:8000/" gives you the main page, where you can search an IP
"http://120.0.0.1:8000/previous/" shows a JSON with all previous searches
